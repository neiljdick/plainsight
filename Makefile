CC = gcc

CFLAGS = -g -c -Wall -std=c99 -Werror 
EXE = plainsight 

all: $(EXE) 

$(EXE): plainsight.o 
	$(CC) -o $(EXE) plainsight.o 

plainsight.o:
	$(CC) $(CFLAGS) plainsight.c 
clean:
	rm *.o $(EXE)
