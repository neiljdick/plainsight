# Plainsight

Hide Files within other files!

This program will take an input file, slice it into evenly sized bits, and
write it into one or more other files. It generates a report of what it's done 
in a file called 'receipt.txt'. The output files are shuffled so that 
the input file is strewn randomly amogst the outputs.

You can then recover the input file using this receipt and the output files. 



## Example Usage:
**Warning - Output files will remain corrupted after recovery so make a copy first**
- Hide Secret.doc in several music files.

`./plainsight -i Secret.doc -o The\ Clash/*.mp3`
-  Recover Secret.doc

`./plainsight -R receipt.txt -o Recovered.doc`
* For bonus points you can encrypt the file before hand. 

`gpg -c Secret.doc`

`./plainsight -i Secret.doc.gpg -o The\ Simpsons/*.avi`

* Then Decrypt it after recovery

`./plainsight -R receipt.txt -o Recovered.doc.gpg`

`gpg Recovered.doc.gpg`