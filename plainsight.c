#define _BSD_SOURCE // this is needed on my server
#define _DEFAULT_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include <stdint.h>
#include <time.h>

#define OUTFILENAME "receipt.txt"

static inline int HALFFILE(int size)
{
    return ((size<<1)+0x02)>>2;
}

int HandleCmdLine(int argc, char **argv);
void PrintHelp(void);
size_t FileSize(int In);
void RecoverFile();
void HideFile();
void HideFileChunk(int In,
                   int Out,
                   int InSliceLow,
                   int InSliceHigh,
                   int OutOffset);
void RecoverFileChunk(int In,
                      int Out,
                      int InOffset,
                      int OutSize);
int FindChunkSize(int Index,
                  int InFileSize,
                  int NumOutFiles,
                  int *pLow,
                  int *pHigh);
void ShuffleIndexes(int Buf[], size_t Len);


/* Cutting corners */
char *pInFileName;
char **ppOutFileNames;
char *pReceiptName;
unsigned int NumOutFiles = 0;
int Recover = 0;

int main(int argc, char **argv)
{
    pInFileName = NULL;
    ppOutFileNames = NULL;
    pReceiptName = NULL;

    if (argc < 5)
    {
        PrintHelp();
        return -1;
    }

    HandleCmdLine(argc, argv);
    assert(ppOutFileNames);

    /* Seed RNG */
    srandom(time(NULL));

    if (Recover)
    {
        RecoverFile();
    }
    else
    {
        HideFile();
    }

}

void HideFile()
{
    /* this is kind of confusing, create a scrambled array of indexes */
    /* so the files get opened in a random order */
    int Scramble[NumOutFiles];
    assert(pInFileName);
    int InFd = open(pInFileName, O_RDWR);
    assert(InFd != -1);
    FILE  *Receipt = fopen(OUTFILENAME, "w");

    assert(Receipt);

    for (int i = 0; i < NumOutFiles; i++)
    {
        Scramble[i] = i;
    }
    ShuffleIndexes(Scramble, NumOutFiles);


    /* Open the output files in scrambled order */
    int RecLen = 0;
    char **ppRecOutFile = malloc(sizeof(char *)*NumOutFiles);
    assert(ppRecOutFile);
    for (int i = 0; i < NumOutFiles; i++)
    {
        int InLow, InHigh = 0;
        int OutFd = open(ppOutFileNames[Scramble[i]], O_RDWR | O_CREAT, S_IRWXU);   
        assert(OutFd != -1);
        size_t OutSize = FileSize(OutFd);
        size_t InSize = FileSize(InFd);

        /* Work out a chunk */
        if (FindChunkSize(i, InSize, NumOutFiles, &InLow, &InHigh))
        {
            ppRecOutFile[RecLen] = malloc(512);
            assert(ppRecOutFile[RecLen]);

            sprintf(ppRecOutFile[RecLen], "%s %d %d \n", ppOutFileNames[Scramble[i]], (int)HALFFILE(OutSize), (InHigh-InLow));
            /* Hide the chunk within the output file */
            HideFileChunk(InFd, OutFd, InLow, InHigh, HALFFILE(OutSize));
            ++RecLen;
        }
        close(OutFd);
    }
    char Buf[32];
    snprintf(Buf, sizeof(Buf), "%d\n", RecLen);
    /* Create the Receipt File */
    fputs(Buf, Receipt);
    for (int i=0; i<RecLen; ++i)
    {
        fputs(ppRecOutFile[i], Receipt);
        free(ppRecOutFile[i]);
    }
    free(ppRecOutFile);

    close(InFd);
    fclose(Receipt);
}



void RecoverFile()
{
    assert(pReceiptName);
    int OutFd = open(ppOutFileNames[0], O_RDWR | O_CREAT, S_IRWXU); 
    FILE *RecFd = fopen(pReceiptName, "r");
    int N = 0;
    fscanf(RecFd, "%d", &N);
    assert(RecFd);
    for (int i=0; i<N; ++i)
    {
        char InName[1024];
        int Offset,Size;
        fscanf(RecFd, "%s %d %d", InName, &Offset, &Size);
        int InFd = open(InName, O_RDWR);
        assert(InFd != -1);
        RecoverFileChunk(InFd, OutFd, Offset, Size);
        close(InFd);
    }
    close(OutFd);
    fclose(RecFd);
}

int FindChunkSize(int Index,
                  int InFileSize,
                  int NumOutFiles,
                  int *pLow,
                  int *pHigh)
{
    assert(pLow);
    assert(pHigh);
    assert(NumOutFiles);
    assert(InFileSize);
    int Ret = 0;
    /* If there isn't enough data to split between given files
       just split the data into the first few files
    */
    int OutFiles = (NumOutFiles < InFileSize ? NumOutFiles : InFileSize);
    if (Index < OutFiles)
    {
        int ChunkSize = InFileSize/OutFiles;
        *pLow = Index * ChunkSize;
        *pHigh = (*pLow)+ChunkSize;

        /* If there is a remainder just whack it into the last file */
        if (Index == (OutFiles-1) && (InFileSize % OutFiles))
        {
            *pHigh += InFileSize % OutFiles;
        }
        Ret = 1;
    }
    return Ret;
}


void RecoverFileChunk(int In,
                      int Out,
                      int InOffset,
                      int OutSize)
{
    int BytesToRead = OutSize;
    lseek(In, InOffset, SEEK_SET);  
    while (BytesToRead > 0)
    {
        char OutBuf[1024];
        int ReadSize = (BytesToRead < sizeof(OutBuf) ? BytesToRead : sizeof(OutBuf));
        int n = read(In, OutBuf, ReadSize);
        write(Out, OutBuf, n);
        BytesToRead -= n;
    }
    return;
}


void HideFileChunk(int In,
                   int Out,
                   int InSliceLow,
                   int InSliceHigh,
                   int OutOffset)
{
    int BytesToCopy = InSliceHigh - InSliceLow;
    size_t OutSize = FileSize(Out);
    assert(OutSize >= OutOffset);
    /* Allocate buffer to hold the tail of the output file. */
    char *pOutRemainder = malloc(OutSize-OutOffset);
    assert(pOutRemainder);

    
    /* seek the Input to the desired offset */
    lseek(In, InSliceLow, SEEK_SET);

    /* seek the output to the desired offset */
    lseek(Out, OutOffset, SEEK_SET);
    /* Copy the tail of the output file into RAM */
    int BytesRead = read(Out, pOutRemainder, OutSize-OutOffset);
    assert(BytesRead == (OutSize-OutOffset));
    /* Seek back to the right spot */
    lseek(Out, OutOffset, SEEK_SET);    

    while (BytesToCopy > 0)
    {
        char InBuf[1024];
        int BytesToRead = (BytesToCopy < sizeof(InBuf) ? BytesToCopy : sizeof(InBuf));
        int BytesIn = read(In, InBuf, BytesToRead);
        write(Out, InBuf, BytesIn);
        BytesToCopy-=BytesIn;
    }
    /* Write the tail of the output file back */
    write(Out, pOutRemainder, BytesRead);

    free(pOutRemainder);
    return;
}


size_t FileSize(int In)
{
    struct stat Buf;
    size_t Size = 0;
    if (fstat(In, &Buf) != -1)
    {
        Size = (size_t)Buf.st_size;
    }
    else
    {
        assert(0);
    }
    return Size;
}

void ShuffleIndexes(int Buf[], size_t Len)
{
    /* shuffle algorithm from google
       for each element in the array
       roll a random number within array bounds,
       swap that element with current element.
    */
    int Tmp;
    for (int i=0; i<Len; i++)
    {
        int Ind = random()%Len;
        Tmp = Buf[i];
        Buf[i] = Buf[Ind];
        Buf[Ind] = Tmp;
    }
}

int HandleCmdLine(int argc, char **argv)
{
    for (int i = 0; i < argc; i++)
    {
        if (!strcmp(argv[i], "-i"))
        {
            pInFileName = argv[i+1];
        }
        else if (!strcmp(argv[i], "-o"))
        {
            /* TODO - figure out how to do this properly? */
            /* Assuming everything after -o is a filename we want to hide in */
            ppOutFileNames = malloc(sizeof(char *)*(argc-i-1));
            assert(ppOutFileNames);
            int Index = 0;
            for (int j = i+1; j < argc; j++)
            {
                ppOutFileNames[Index++] = argv[j];
            }
            NumOutFiles = Index;
            break;
        }
        else if (!strcmp(argv[i], "-R"))
        {
            Recover = 1;
            pReceiptName = argv[i+1];
        }
    }
    return 0;
}


void PrintHelp(void)
{
    printf("Usage: \n \
Hiding Files: \n\
./plainsight -i infile -o outfile\n\
Recovering Files\n\
./plainsight -R receipt.txt  -o recoveredName\n\
Currently the Argument handling is very poor and the -o parameter must appear last\n");
    return;
}
